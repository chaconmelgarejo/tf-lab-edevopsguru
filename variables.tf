##################################################################################
# VARIABLES
##################################################################################

variable "private_key_path" {}
variable "profile" {}
variable "key_name" {}
variable "region" {
  default = "us-east-1"
}
variable "network_address_space" {}
variable "instance_size" {}
variable "subnet_count" {}
variable "instance_count" {}
variable "instance_name" {}

##################################################################################
# LOCALS
##################################################################################

locals {
  env_name = "dev"
    common_tags = {
    Environment = local.env_name

  }
}