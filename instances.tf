# INSTANCES #
resource "aws_instance" "lab" {
  count                  = var.instance_count
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_size
  subnet_id              = aws_subnet.subnet[0].id
  vpc_security_group_ids = [aws_security_group.ansible-sg.id]
  key_name               = var.key_name
  
  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file(var.private_key_path)

  }

  provisioner "remote-exec" {
    inline = [
        #"sudo apt-add-repository ppa:ansible/ansible",
        "sudo apt update"
    ]
  }

  tags = merge(local.common_tags, { Name = "${var.instance_name[count.index]}" })
}